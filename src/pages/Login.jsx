import { Container, Grid } from "@mui/material"
import LoginContent from "../components/loginContent"
const Login = () => {
    return (
      
      <Container maxWidth="100%" className="bgLogin">
          <Grid container>
              <Grid item sm={6} md={6} lg={6} xl={6} xxl={6} className="loginContainer">
                  <LoginContent/>
              </Grid>
          </Grid>
      </Container>
      
    )
}

export default Login