import { Person2Outlined, VisibilityOffOutlined, VisibilityOutlined } from "@mui/icons-material"
import { Box, FilledInput, FormControl, IconButton, InputAdornment, InputLabel, Typography,Button } from "@mui/material"
import React from "react";

const LoginContent = () => {
    const [showPassword, setShowPassword] = React.useState(false);

    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };
    return (
        <>
            <Box className="loginBox text-center">
                <Typography variant="h4" className="loginTitle">
                    Đăng nhập
                </Typography>
                <FormControl fullWidth= "100%" sx={{ mt: "35px" }} variant="filled">
                    <InputLabel htmlFor="username">
                        Tên tài khoản
                    </InputLabel>
                    <FilledInput
                        sx={{ color: "#F5F5F5" }}
                        id="username"
                        endAdornment={
                            <InputAdornment position="end">
                                <Person2Outlined />
                            </InputAdornment>
                        }
                    />
                </FormControl>
                <FormControl fullWidth= "100%" sx={{ mt: "35px"  }} variant="filled">
                    <InputLabel htmlFor="filled-adornment-password">Password</InputLabel>
                    <FilledInput
                        id="filled-adornment-password"
                        type={showPassword ? 'text' : 'password'}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                    edge="end"
                                >
                                    {showPassword ? <VisibilityOutlined/> : < VisibilityOffOutlined/>}
                                </IconButton>
                            </InputAdornment>
                        }
                    />
                </FormControl>
                <Button sx={{ mt: '35px', width: '100%' }} className="loginButton" variant="contained">Login</Button>
            </Box>
        </>

    )
}
export default LoginContent